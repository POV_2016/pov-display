: delay_1ms
d# 8334 begin d# 1 - dup d# 0 = until \ 100MHz
;

: delay_ms
d# 0 do delay_1ms loop	
;
: delay_min
d# 60000 delay_ms
;
: main
begin
d# 65 uart_data !
begin
uart_busy @
d# 0 =
until
d# 2 delay_min
d# 66 uart_data !
d# 2 delay_min
d# 67 uart_data !
d# 2 delay_min
again 
;
