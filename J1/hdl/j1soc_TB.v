`timescale 1ns / 1ps
`define SIMULATION
// ============================================================================
// TESTBENCH FOR TINYCPU
// ============================================================================

module j1soc_TB ();

reg 	sys_clk_i, sys_rst_i;
wire  	uart_tx, ledout; 
wire 	[15:0] gp_out0, gp_out1;
reg 	[15:0] gp_in0, gp_in1;

j1soc uut (
	 uart_tx, ledout, gp_out0, gp_out1, gp_in0, gp_in1, sys_clk_i, sys_rst_i
);
parameter stop_time = 2000000;
initial begin
  sys_clk_i   = 1;
  sys_rst_i = 1;
  #10 sys_rst_i = 0;
  gp_in0 = 16'd12;
  gp_in1 = 16'd465;

end

always sys_clk_i = #5 ~sys_clk_i;   //100MHz


//always sys_clk_i = #10 ~sys_clk_i;    //50MHz


initial begin: TEST_CASE
  $dumpfile("j1soc_TB.vcd");
  $dumpvars(-1, uut);
  # stop_time $finish;
 // #10000000 $finish;
end

endmodule
