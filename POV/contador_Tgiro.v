module contador_Tgiro (Periodo, Clk, Sensor);

parameter dataWidth = 32;

output reg [dataWidth-1: 0] Periodo;

input Clk, Sensor;

reg done;

reg [dataWidth-1: 0] salida;
reg [dataWidth-1: 0] save;

initial begin
done=0;
save= 0;
Periodo =0;
salida =0;
end

always @(posedge Clk, negedge Sensor)

if(~Sensor) begin	
	salida=0;
	done=1;
	end

else if(Sensor) begin
	salida = salida + 1;
	save=salida;
	done=0;
	end
 	
always @(posedge done)
begin
Periodo = save;
end
endmodule
