module control_led( clk , rst , init , done , data , out, address, AD );


 input clk;
 input rst;
 input init; 
 input [23:0] data;
 input [7:0]AD;

 output reg done;
 output reg out;
 output reg [7:0] address;
 

 parameter READY  = 3'b000;
 parameter READ  = 3'b001;
 parameter H0  = 3'b010;
 parameter L0  = 3'b011;
 parameter H1  = 3'b100;
 parameter L1  = 3'b101;
 parameter RESET  = 3'b110;
 parameter T0H = 18;
 parameter T1H = 35;
 parameter T0L = 40;
 parameter T1L = 30;
 parameter RES = 5000;
 
 reg [2:0] state;
 reg [7:0] timer;
 reg [4:0] i;
 reg en;
 
 
 initial begin
  done = 0;
  state = READY;
  timer= 0;
  i=0;
  out=0;
  address = 8'h00;
  en = init;
 end

reg [3:0] count;

always @(posedge clk) begin
    
    if (rst) begin
      state = READY;
    end else begin
    case(state)

     READY:begin
	i=0;
	en = init; 
	done=0;
	   if(en)begin
	     en=0;
          state = READ;
          address = AD;
          end
        else
          state = READY;
     end

     READ:	
      if(data[i])begin
        state = H1;
        timer = T1H;
        done=0;
        end
      else
        begin     
        state = H0;
        timer = T0H;
        done=0;
        end
     H0: begin
     done=0;
     out = 1'bz;
     timer=timer-1;
      if(timer==0)begin
        state = L0;
        timer = T0L;
        end
      else
        state = H0; 
     end    
     L0: begin
     done=0;
     out=0;
     timer=timer-1;
      if(timer==0)begin
 	   i = i+1;
 	   if (i==24)begin	     
        state = RESET;
        timer = RES;
        end
        else state = READ;
        end
      else
        state = L0;
     end   
     H1: begin
     done=0;
     out = 1'bz;
     timer=timer-1;
      if(timer==0)begin
        state = L1;
        timer = T1L;
        end
      else
        state = H1;
     end
     L1: begin
     done=0;
     out=0;
     timer=timer-1;
      if(timer==0)begin
 	   i = i+1;
 	   if (i==24)begin	     
        state = RESET;
        timer = RES;
        end
        else state = READ;
        end
      else
        state = L1;
     end
     RESET: begin
	 i=0;     
	 done = 1;   
	 address=address+1;
      state = READY;   
      
     end

     default: state = READY;
     
   endcase
   end
 end

endmodule
