module maquina_de_estados( clk , rst , init_l , init, address, diseno, Periodo, done_leds, sensor );


 input clk;
 input rst;
 input init; 
 input [7:0] diseno;
 input [31:0] Periodo;
 input sensor;
 input done_leds;
 
 output reg init_l;
 output reg [7:0] address;
 

 parameter READY  = 3'b000;
 parameter START  = 3'b001;
 parameter DEFAULT  = 3'b010;
 parameter DESING = 3'b011;
 parameter WAIT  = 3'b100;
 parameter SEND = 3'b101;
 parameter FINISH = 3'b110;
 
 parameter CERO  = 3'b000;
 parameter UNO  = 3'b001;
 parameter DOS  = 3'b010;
 parameter TRES = 3'b011;
 
 reg [2:0] state;
 reg [25:0] timer;
 reg [7:0] AD_in;
 reg [7:0] AD_f;
 reg new;
 reg [4:0]  led_num;
 reg stop;

 initial begin
  state = READY;
  timer= 0;
  init_l=0;
  address <= 0;
  AD_in =0;
  AD_f=0;
  new=0;
  led_num=0;

 end

always @(negedge clk) begin
    if (rst) begin
      state = READY;
      timer= 0;
  	 init_l=0;
  	 address  = 0;
    end 
    else if (~sensor)begin
 	 state = WAIT;
 	 timer =5000;
 	 init_l=0;    
 	 address  = 0;
 	 stop=1;
    end
    else begin
    case(state)

     READY:begin
        if(init)
          state = START;
        else
          state = READY;
     end

     START: begin
      	
      state=DEFAULT;
     	end
     DEFAULT: begin
      
      case (diseno[2:0]) 
      
      CERO: begin
	 AD_in=0;
	 AD_f=8'b1010100;	
	 state = DESING;
	 new=1;
	 end

      UNO: begin
	 AD_in=0;
	 AD_f=8'b1010100;	
	 state = DESING;
	 new=1;
	 end
	 
      DOS: begin
	 new=1;
	 AD_in=8'b1010100;
	 AD_f=8'b10101000;	
	 state = DESING;
	 end
	 
      TRES: begin
	 new=1;
	 AD_in=0;
	 AD_f=8'b110000;	
	 state = DESING;
	 end 
   
      default: begin 
      new=1;
	 AD_in=8'b110000;
	 AD_f=8'b1100000;	
	 state = DESING;
      end
      endcase 
      
      end
       
     DESING: begin
		if(new)begin 
			address = AD_in;
			new =0;
	    	end else if (address == AD_f) begin
			state = READY;
	    	end else begin
		    timer = Periodo[31:7];
		    init_l=1;
		    led_num=0;    
		    state = SEND;
		end
	end
     
     WAIT: begin
     init_l = 0;
     
     timer = timer-1;
      if(timer==0)begin
        state = FINISH;
        end 
      else
        state = WAIT;
      end
     
     SEND:            
     if(led_num == 13)
		if(done_leds) begin
			address = address + 1;
		     state = WAIT;
		     led_num=0;
		end else begin
			init_l=0;  
			state = SEND;     
		end
     else begin
     
     	if(done_leds) begin
     	init_l = 1; 
     	led_num = led_num + 1;
     	address = address + 1;
     	state = SEND;
          end
     	else begin
     	state = SEND; 
     	init_l=0;
     	end
     end  
     
     FINISH:
     	if(init)
     	state = FINISH;
     	else if(stop) begin
     	stop=0;
     	state = DEFAULT;
     	end else begin
     	state = DESING;
     	end
      
   default: state = READY;  
   endcase
   end
 end

endmodule
