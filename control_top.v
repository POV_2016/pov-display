//`default_nettype none

//`include "baudgen.vh"

module control_top(clk1, senal, init, rst, done, rx, sensor, add);
//  localparam BAUD = `B9600;
  
  input clk1;
  input sensor;
  output senal;
  output done;
  input rst;
  input init;
  input rx;
  output [7:0] add;

  wire [7:0] w_AD;
  wire [7:0] AD;
  wire [23:0] w_linea; 
  wire init_l;
  wire [31:0] Periodo;
  wire debounce;
  wire [7:0] dis;
  
  assign add = dis;
  
  contador_Tgiro Count (.Periodo(Periodo), .Clk(clk1), .Sensor(sensor));
  debounce deb1 (.inp(init), .cclk(clk1), .clr(rst), .outp(debounce)); 
  maquina_de_estados M1( .clk(clk1) , .rst(rst) , .init_l(init_l) , .init(debounce), .address(AD), .diseno(dis), .Periodo(Periodo), .done_leds(done), .sensor(sensor) );  
  rom_using_file rom0 (.address(w_AD), .data(w_linea));
  control_led C1 (.clk(clk1), .rst(rst), .init(init_l), .done(done), .data(w_linea),.out(senal), .address(w_AD), .AD(AD));
  rxleds disen0 (.clk(clk1),.rx(rx),.leds(dis));

endmodule
