`timescale 1ns / 1ps

`define SIMULATION

module control_top_TB;

reg t_clk;
reg t_rst;
reg t_init;
reg rx;
reg t_sensor;
wire t_done;
wire t_out;
wire t_led;
wire [7:0]t_add;
parameter stop_time = 10000000;

control_top C1 (t_clk, t_out, t_init, t_rst, t_done, rx, t_sensor, t_led, t_add);

initial # stop_time $finish;

initial begin: TEST_STIMULUS
	
	t_clk=1'b0;
	t_init=1'b0;
	t_rst=1'b0;
	//
	t_sensor=1;	
	
#200
	t_init=1'b1;
	t_sensor=1;
#200
	t_init=1'b0;
	t_sensor=1;
#200000
	t_init=1'b1;
	t_sensor=0;
#300010
	t_init=1'b1;
	t_sensor=1;

#30000
	//t_diseno=3'b010;
	
	t_init=1'b0;
	t_sensor=1;
#5000010
	//t_diseno=3'b001;
	//t_init=1'b1;
	t_sensor=1;

#5000
	t_init=1'b0;
end
always 
#10 t_clk = ~t_clk;

initial # stop_time $finish;

initial begin: TEST_MONITOR


end


initial # stop_time $finish;

initial begin: TEST_WAVE

$dumpfile("control_top_TB.vcd");

$dumpvars (-1,C1);

end


endmodule
