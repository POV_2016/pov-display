//-----------------------------------------------------
// Design Name : rom_using_file
// File Name   : rom_using_file.v
// Function    : ROM using readmemh
// Coder       : Deepak Kumar Tala
//-----------------------------------------------------
module rom_using_file (
address , // Address input
data    , // Data output
read_en , // Read Enable 
ce        // Chip Enable
);
input [7:0] address;
output [23:0] data; 
input read_en; 
input ce; 
           
reg [23:0] mem [0:168] ;  

assign data = mem[address] ;

initial begin
  $readmemb("memory.list", mem); // memory_list is memory file
 // read_en <= 1'b1;
  //ce <= 1'b1;
end

endmodule

